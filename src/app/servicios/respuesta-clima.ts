export interface RespuestaClima {
    count: number,
    data: Array<any>
}

export interface Clima {
    icon: string,
    code: number,
    description: string,
}

export interface Detalle {
    weather: Clima,
    temp: number,
    wind_spd: number,
    city_name: string
}