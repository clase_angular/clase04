import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RespuestaClima } from './respuesta-clima';

@Injectable({
  providedIn: 'root'
})
export class ClimaService {

  constructor(private httpClient: HttpClient) { }

  obtenerClima(lugar: string): Observable<RespuestaClima> {
    let url: string = "https://api.weatherbit.io/v2.0/current";
    let params = new HttpParams({
      fromObject: {
        city: lugar,
        country: 'ES',
        lang: 'es',
        key: 'feb13e9e859645b588607bf4e2bb1a08'
      }
    });
    let headers = new HttpHeaders({'Accept': 'application/json'});

    return this.httpClient.get<RespuestaClima>(url, {
      params: params,
      headers: headers
    });
  }

}
