import { Component, OnDestroy, OnInit } from '@angular/core';
import { ClimaService } from 'src/app/servicios/clima.service';
import { RespuestaClima } from 'src/app/servicios/respuesta-clima';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  ciudad: string = "";
  temperatura: string = "";
  descripcion: string = "";
  viento: number = 0;
  imagen: string = "";

  constructor(private climaSrv: ClimaService) { }

  ngOnInit(): void {
  }

  buscarClima(lugar: string) {
    this.climaSrv.obtenerClima(lugar)
      .subscribe(
        (response: RespuestaClima) => {
          this.ciudad = response.data[0].city_name;
          this.temperatura = `${response.data[0].temp}°C`;
          this.imagen = `https://www.weatherbit.io/static/img/icons/${response.data[0].weather.icon}.png`;
          this.descripcion = response.data[0].weather.description;
          this.viento = response.data[0].wind_spd;
        },
        (error) => {},
      );
  }

}
